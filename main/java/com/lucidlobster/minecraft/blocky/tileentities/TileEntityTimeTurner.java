package com.lucidlobster.minecraft.blocky.tileentities;

import net.minecraft.tileentity.TileEntity;

public class TileEntityTimeTurner extends TileEntity {

	int tick = 0;
	
	public static final String publicName = "tileEntityTimeTurner";
    private String name = "tileEntityTimeTurner";
 
    public String getName() {
 
        return name;
    }
	 	
	@Override
	public void updateEntity() {
		if(!worldObj.isRemote) {
			tick++;
			if(tick == 100)
			{
				worldObj.setWorldTime(1000);
				tick = 0;
			}
		}
	}
	
}
