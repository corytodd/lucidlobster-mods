package com.lucidlobster.minecraft.blocky.projectile;

import java.util.Random;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.projectile.EntityThrowable;
import net.minecraft.util.DamageSource;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;

public class EntityNinjaStar extends EntityThrowable {

	public EntityNinjaStar(World world) {
		super(world);
	}

	public EntityNinjaStar(World world, EntityLivingBase entityLivingBase) {
		super(world, entityLivingBase);
	}

	public EntityNinjaStar(World world, double p1, double p2, double p3) {
		super(world, p1, p2, p3);
	}

	/**
	 * Called when this EntityThrowable hits a block or entity. Will cause 10 damage to anything it hits
	 */
	protected void onImpact(MovingObjectPosition movingObjectPosition) {
		
		if (movingObjectPosition.entityHit != null) {
			byte b0 = 10;
			movingObjectPosition.entityHit.attackEntityFrom(
					DamageSource.causeThrownDamage(this, this.getThrower()),
					(float) b0);
		}

		Random rng =  new Random();
		
		// random number between 0 and 2 (inclusive) == 33% chance of exploding
		if(rng.nextInt(3) == 0)
		{
			// Causes 8 random explosions upon impact... one for each point of the star
			for (int i = 0; i < 8; ++i) {
		
				this.worldObj.newExplosion(this, movingObjectPosition.blockX,
					movingObjectPosition.blockY, movingObjectPosition.blockZ,
					rng.nextFloat()*4, true, true);
			}
		}

		if (!this.worldObj.isRemote) {
			this.setDead();
		}

	}

}
