package com.lucidlobster.minecraft.blocky.projectile;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.projectile.EntityThrowable;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;


public class EntityZeusBolt extends EntityThrowable {

	public EntityZeusBolt(World world) {
		super(world);
	}

	public EntityZeusBolt(World world, EntityLivingBase entityLivingBase) {
		super(world, entityLivingBase);
	}

	public EntityZeusBolt(World world, double p1, double p2, double p3) {
		super(world, p1, p2, p3);
	}

	/**
	 * Called when this EntityThrowable hits anything. Causes brief rain/snow depending on elevation.
	 */
	protected void onImpact(MovingObjectPosition movingObjectPosition) {
	
		try{
			this.worldObj.setRainStrength(10F);
		}catch(Exception e){ /* Not sure why this seems to not exist on some servers */}

		if (!this.worldObj.isRemote) {
			this.setDead();
		}

	}

}
