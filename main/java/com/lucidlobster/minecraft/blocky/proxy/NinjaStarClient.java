package com.lucidlobster.minecraft.blocky.proxy;

import net.minecraft.client.renderer.entity.RenderSnowball;

import com.lucidlobster.minecraft.blocky.items.ModItems;
import com.lucidlobster.minecraft.blocky.projectile.EntityNinjaStar;

import cpw.mods.fml.client.registry.RenderingRegistry;

public class NinjaStarClient extends NinjaStarCommon {
	
	@Override
	public void registerRenderThings() {
		RenderingRegistry.registerEntityRenderingHandler(EntityNinjaStar.class, new RenderSnowball(ModItems.ninjaStar));
	}
	
	@Override
	public void registerSounds(){}

}
