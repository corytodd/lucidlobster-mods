package com.lucidlobster.minecraft.blocky.proxy;

import com.lucidlobster.minecraft.blocky.tileentities.TileEntityTimeTurner;

import cpw.mods.fml.common.registry.GameRegistry;

public class TimeTurnerCommon {
	
	public void registerTileEntities() {
		GameRegistry.registerTileEntity(TileEntityTimeTurner.class, TileEntityTimeTurner.publicName);
	}

}
