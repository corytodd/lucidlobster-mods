package com.lucidlobster.minecraft.blocky.proxy;

import net.minecraft.client.renderer.entity.RenderSnowball;

import com.lucidlobster.minecraft.blocky.items.ModItems;
import com.lucidlobster.minecraft.blocky.projectile.EntityZeusBolt;

import cpw.mods.fml.client.registry.RenderingRegistry;

public class ZeusBoltClient extends ZeusBoltCommon {
	
	@Override
	public void registerRenderThings() {
		RenderingRegistry.registerEntityRenderingHandler(EntityZeusBolt.class, new RenderSnowball(ModItems.zeusBolt));
	}
	
	@Override
	public void registerSounds(){}

}
