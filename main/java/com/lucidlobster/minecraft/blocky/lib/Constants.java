package com.lucidlobster.minecraft.blocky.lib;

public class Constants {

	public static final String MODID = "blocky";
	public static final String MODNAME = "Blocky";
	public static final String VERSION = "1.0.0.0";
	
	public static final String TIMETURNER_CLIENT_PROXY = "com.lucidlobster.minecraft.blocky.proxy.TimeTurnerClient";
	public static final String TIMETURNER_COMMON_PROXY = "com.lucidlobster.minecraft.blocky.proxy.TimeTurnerCommon";

	public static final String NINJASTAR_CLIENT_PROXY = "com.lucidlobster.minecraft.blocky.proxy.NinjaStarClient";
	public static final String NINJASTAR_COMMON_PROXY = "com.lucidlobster.minecraft.blocky.proxy.NinjaStarCommon";
	
	public static final String ZEUSBOLT_CLIENT_PROXY = "com.lucidlobster.minecraft.blocky.proxy.ZeusBoltClient";
	public static final String ZEUSBOLT_COMMON_PROXY = "com.lucidlobster.minecraft.blocky.proxy.ZeusBoltCommon";

}
