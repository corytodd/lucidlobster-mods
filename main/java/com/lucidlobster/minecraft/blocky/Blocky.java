package com.lucidlobster.minecraft.blocky;

import com.lucidlobster.minecraft.blocky.blocks.ModBlocks;
import com.lucidlobster.minecraft.blocky.items.ModItems;
import com.lucidlobster.minecraft.blocky.lib.Constants;
import com.lucidlobster.minecraft.blocky.projectile.EntityNinjaStar;
import com.lucidlobster.minecraft.blocky.projectile.EntityZeusBolt;
import com.lucidlobster.minecraft.blocky.proxy.NinjaStarClient;
import com.lucidlobster.minecraft.blocky.proxy.NinjaStarCommon;
import com.lucidlobster.minecraft.blocky.proxy.ZeusBoltClient;
import com.lucidlobster.minecraft.blocky.proxy.ZeusBoltCommon;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.registry.EntityRegistry;

@Mod(modid = Constants.MODID, name = Constants.MODNAME, version = Constants.VERSION)
public class Blocky {

		
	//@SidedProxy(clientSide = Constants.TIMETURNER_CLIENT_PROXY, serverSide = Constants.TIMETURNER_COMMON_PROXY)
	//public static TimeTurnerCommon timeTurnerProxy;
	
	@SidedProxy(clientSide = Constants.NINJASTAR_CLIENT_PROXY, serverSide = Constants.NINJASTAR_COMMON_PROXY)
	public static NinjaStarCommon ninjaStarProxy;
	
	@SidedProxy(clientSide = Constants.ZEUSBOLT_CLIENT_PROXY, serverSide = Constants.ZEUSBOLT_COMMON_PROXY)
	public static ZeusBoltCommon zeusBoltProxy;
	
	@Mod.EventHandler
	public void preInit(FMLPreInitializationEvent event) {
		
		ModBlocks.init();
		ModItems.init();
		
	}
	
	@Mod.EventHandler
	public void init(FMLInitializationEvent event) {
		//timeTurnerProxy.registerTileEntities();		
				
		EntityRegistry.registerModEntity(EntityNinjaStar.class, "NinjaStar", 4, this, 80, 3, true);
		
		ninjaStarProxy.registerRenderThings();
		ninjaStarProxy.registerSounds();
		
		EntityRegistry.registerModEntity(EntityZeusBolt.class, "ZeusBolt", 5, this, 81, 4, true);
		
		zeusBoltProxy.registerRenderThings();
		zeusBoltProxy.registerSounds();
	}
	
	@Mod.EventHandler
	public void postInit(FMLPostInitializationEvent event){
		
	}
	
	
	
}
