package com.lucidlobster.minecraft.blocky.items;

import net.minecraft.item.Item;

public final class ModItems {
	
	public static Item ninjaStar;
	public static Item zeusBolt;
	
	public static void init() {
		
		ninjaStar = new ItemNinjaStar();
		zeusBolt = new ItemZeusBolt();
	}

}
