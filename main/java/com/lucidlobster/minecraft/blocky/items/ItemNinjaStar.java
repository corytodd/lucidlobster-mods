package com.lucidlobster.minecraft.blocky.items;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

import com.lucidlobster.minecraft.blocky.lib.Constants;
import com.lucidlobster.minecraft.blocky.projectile.EntityNinjaStar;

import cpw.mods.fml.common.registry.GameRegistry;

public class ItemNinjaStar extends Item {

	private String name = "itemNinjaStar";
	
	/*
	 * A throwable item that causes damage and has a chance to cause explosive damage
	 */
	public ItemNinjaStar() {
		
        this.maxStackSize = 64;        
		setUnlocalizedName(name);
		setTextureName(Constants.MODID + ":" + name);
		setCreativeTab(CreativeTabs.tabCombat);
		GameRegistry.registerItem(this, name);
		
	}
	
    /**
     * Called whenever this item is equipped and the right mouse button is pressed. Args: itemStack, world, entityPlayer
     */
    public ItemStack onItemRightClick(ItemStack itemStack, World world, EntityPlayer entityPlayer)
    {
        if (!entityPlayer.capabilities.isCreativeMode)
        {
            --itemStack.stackSize;
        }

        world.playSoundAtEntity(entityPlayer, "tile.piston.out", 0.5F, 0.4F / (itemRand.nextFloat() * 0.4F + 0.8F));

        if (!world.isRemote)
        {
        	world.spawnEntityInWorld(new EntityNinjaStar(world, entityPlayer));
        }

        return itemStack;
    }
	
}
