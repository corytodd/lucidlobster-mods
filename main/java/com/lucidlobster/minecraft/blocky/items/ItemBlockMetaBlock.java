package com.lucidlobster.minecraft.blocky.items;

import net.minecraft.block.Block;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;

public class ItemBlockMetaBlock extends ItemBlock {

	public ItemBlockMetaBlock(Block block) {
		super(block);
		setHasSubtypes(true);
	}
	
	@Override
	public String getUnlocalizedName(ItemStack itemStack) {
		
		String name;
		switch(itemStack.getItemDamage()) {
			case 0: name = "firstMeta"; break;
			case 1: name = "secondMeta"; break;
			case 2: name = "thridMeta"; break;
			default: name = "Lucidlobster"; break;
		}
		
		return getUnlocalizedName() + "." + name;
	}
	
	@Override
	public int getMetadata(int meta) {
		return meta;
	}
	
}
