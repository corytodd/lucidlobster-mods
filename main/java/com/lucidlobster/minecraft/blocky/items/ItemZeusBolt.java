package com.lucidlobster.minecraft.blocky.items;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

import com.lucidlobster.minecraft.blocky.lib.Constants;
import com.lucidlobster.minecraft.blocky.projectile.EntityZeusBolt;

import cpw.mods.fml.common.registry.GameRegistry;

public class ItemZeusBolt extends Item {

	private String name = "itemZeusBolt";
	
	public ItemZeusBolt() {
        this.maxStackSize = 64;        
		setUnlocalizedName(name);
		setTextureName(Constants.MODID + ":" + name);
		setCreativeTab(CreativeTabs.tabTools);
		GameRegistry.registerItem(this, name);
	}
	
	 /**
     * Called whenever this item is equipped and the right mouse button is pressed. Args: itemStack, world, entityPlayer
     */
    public ItemStack onItemRightClick(ItemStack itemStack, World world, EntityPlayer entityPlayer)
    {
        if (!entityPlayer.capabilities.isCreativeMode)
        {
            --itemStack.stackSize;
        }

        world.playSoundAtEntity(entityPlayer, "random.fizz", 0.5F, 0.4F / (itemRand.nextFloat() * 0.4F + 0.8F));

        if (!world.isRemote)
        {
        	world.spawnEntityInWorld(new EntityZeusBolt(world, entityPlayer));
        }

        return itemStack;
    }
	
}
