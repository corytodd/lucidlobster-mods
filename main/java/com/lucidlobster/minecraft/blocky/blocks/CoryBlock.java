package com.lucidlobster.minecraft.blocky.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.util.IIcon;

import com.lucidlobster.minecraft.blocky.lib.Constants;

import cpw.mods.fml.common.registry.GameRegistry;

public class CoryBlock extends Block {

	public final String name = "coryBlock";
	
	private IIcon[] icons = new IIcon[6];
	
	public CoryBlock() {
		super(Material.rock);
		this.setCreativeTab(CreativeTabs.tabBlock);
		this.setBlockName(Constants.MODID + "_" + name);
		this.setBlockTextureName(Constants.MODID + ":" + name);
		GameRegistry.registerBlock(this, name);
	}

	@Override
	public void registerBlockIcons(IIconRegister iconRegister){
		for(int i=0; i<icons.length; i++) {
			icons[i] = iconRegister.registerIcon(Constants.MODID + ":" + name + i);
		}
	}
	
	@Override
	public IIcon getIcon(int side, int meta) {
		return icons[side];
	}
}
