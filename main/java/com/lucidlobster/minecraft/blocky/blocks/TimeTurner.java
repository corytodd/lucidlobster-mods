package com.lucidlobster.minecraft.blocky.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

import com.lucidlobster.minecraft.blocky.tileentities.TileEntityTimeTurner;

import cpw.mods.fml.common.registry.GameRegistry;

public class TimeTurner extends Block implements ITileEntityProvider {

	public static String name = "timeTurner";
	private String privateName = "timeTurner";
	
	public TimeTurner() {
		super(Material.rock);
		this.setBlockName(privateName);
		this.setCreativeTab(CreativeTabs.tabBlock);
		GameRegistry.registerBlock(this,  privateName);
	}

	@Override
	public TileEntity createNewTileEntity(World world, int meta) {
		return new TileEntityTimeTurner();
	}
	
	@Override
	public boolean hasTileEntity(int metadata) {
		return true;
	}
}
